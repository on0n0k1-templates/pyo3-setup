//! Integration tests for pyo3-setup
use tempdir::TempDir;

mod funcs;
use funcs::*;

#[test]
fn test_structure() {
    let out = TempDir::new("test_structure").expect("failed to create tempdir for test");
    let path = out.path();

    run_cargo(&["myproject", "--non-interactive"], Some(path));

    let expected_files = [
        "myproject/__init__.py",
        "src/lib.rs",
        ".gitignore",
        "Cargo.toml",
        "MANIFEST.in",
        "pyproject.toml",
        "README.md",
        "requirements.txt",
        "setup.py",
    ];

    check_files_exist(
        format!("{}/myproject", path.display()),
        &expected_files,
        "test_structure",
    );
}
